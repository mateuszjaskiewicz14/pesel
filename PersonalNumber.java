package pesel;

import java.util.ArrayList;
import java.util.Scanner;

public class PersonalNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> pesel = imputNumber(scanner);
        answer(verification(condition(pesel), pesel));

    }

    public static ArrayList<Integer> imputNumber(Scanner scann) {
        ArrayList<Integer> byteNumber = new ArrayList<>();
        int counter = 0;
        System.out.println("Put below all digit of ID");
        String pesel = scann.nextLine();
        while (byteNumber.size() < 11) {
            int check = (int) pesel.charAt(counter) - 48;
            if ((check >= 0 && check <= 9)) {
                byteNumber.add((int) pesel.charAt(counter) - 48);
                counter++;
            } else {
                System.out.println("The entered character is not digit\n" +
                        "Pleas enter agen");
            }
        }
        return byteNumber;
    }

    public static boolean condition(ArrayList<Integer> pesel) {
        // sprawdzenie kolejnści i z zakresu cyfr dal daty urodzenia
            System.out.println("Pleas enter full number year of birth ");
        Scanner scanner = new Scanner(System.in);
            String year = scanner.nextLine();

        int correctingNumber =0 ;
        if((pesel.get(2)==2||pesel.get(2)==3)&&(year.charAt(1)-48==0||year.charAt(1)-48==4)){
            correctingNumber =2;
        }else if((pesel.get(2)==4||pesel.get(2)==5)&&(year.charAt(1)-48==1||year.charAt(1)-48==5)){
            correctingNumber=4;
        }else if((pesel.get(2)==6||pesel.get(2)==7)&&(year.charAt(1)-48==2||year.charAt(1)-48==6)){
            correctingNumber=6;
        }else if((pesel.get(2)==3||pesel.get(2)==3)&&(year.charAt(1)-48==3||year.charAt(1)-48==7)){
            correctingNumber=8;
        }
        boolean out = true;
        for (int i = 0; i < pesel.size(); i++) {
            System.out.print(" " + pesel.get(i));
        }
        System.out.println();
        if (pesel.get(0) < 0 || pesel.get(0) > 9 || (pesel.get(1) > 0 || pesel.get(1) > 9)) {
            out = false;
            System.out.println("Błędny format daty");
            System.out.println("1");
        }

        if (out == true && ((pesel.get(2) - correctingNumber) < 0 || (pesel.get(2) - correctingNumber) > 1 || pesel.get(3) < 0 || pesel.get(3) > 9)) {
            out = false;
            System.out.println("Błędny format daty");
            System.out.println("2");
        }

        if (out == true && ((pesel.get(2) - correctingNumber) == 1 && (pesel.get(3) < 0 || pesel.get(3) > 2))) {
            out = false;
            System.out.println("Błędny format daty");
            System.out.println("3");
        }

        if (out == true && (pesel.get(4) < 0 || pesel.get(4) > 3 || pesel.get(5) < 0 || pesel.get(5) > 9)) {
            out = false;
            System.out.println("Błędny format daty");
            System.out.println("4");
        }

        if (out == true && pesel.get(4) == 3 && (0 > pesel.get(5) || pesel.get(5) > 1)) {
            out = false;
            System.out.println("Błędny format daty");
            System.out.println("44");
        }

        return out;
    }

    public static boolean verification(boolean out, ArrayList<Integer> pesel) {
        int cotrol = 9 * pesel.get(0) + 7 * pesel.get(1) + 3 * pesel.get(2) + 1 * pesel.get(3) + 9 * pesel.get(4) + 7 * pesel.get(5) + 3 * pesel.get(6) + 1 * pesel.get(7) + 9 * pesel.get(8) + 7 * pesel.get(9);
        cotrol %= 10;
        if (out == true && cotrol == pesel.get(10)) {
            if(pesel.get(9)==0){
                System.out.println("Badany obiekt jest samicą człowieka");
            }else System.out.println("Badany obiekt jest samcem człowieka");
            out = true;
        }
        return out;
    }

    public static void answer(boolean condition) {
        if (condition == false) {
            System.out.println("Pesel jest nieprawdziwy");
        } else
            System.out.println("Potwierdzono autentyczność");
    }
}
